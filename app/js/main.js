$(document).ready(function () {
    // $(".preload").delay(500).fadeOut(1000);

    var h = $(window).height();

    // $(".wrap_grey").css('height',h-78);
    $(".scrollable_olimp").css('height',h-110);
    $(".scrollable_rasp").css('height',h-220);
    $(".scrollable_news").css('height',h-110);
    $(".courses_list_wrapper>.scrollable").css('height',h-320);

    $(".video_click").css('height', (h-160)/2);
    $("#course_video").css('height', (h-120));

    function minimizeContent(){
        $(".col4").addClass("col-md-6").removeClass("col-md-4");
        $(".col8").addClass("col-md-6").removeClass("col-md-8");

        $(".courses_content").addClass("col-md-12");
        $(".courses_content").removeClass("col-md-10");

        $(".col5").addClass("col-md-5");
        $(".col5").removeClass("col-md-3");
        $(".col12").addClass("col-md-12");
        $(".col12").removeClass("col-md-10");
        $(".col10").removeClass("col-md-10");
        $(".col10").addClass("col-md-12");
        $(".col10").removeClass("col-sm-10");
        $(".col10").addClass("col-sm-12");
        $(".col65").addClass("col-md-5");
        $(".col65").removeClass("col-md-6");
        $(".col65").addClass("col-sm-5");
        $(".col65").removeClass("col-sm-6");
        $(".col67").addClass("col-md-7");
        $(".col67").removeClass("col-md-6");
        $(".col67").addClass("col-sm-7");
        $(".col67").removeClass("col-sm-6");

    }
    function maximizeContent(){
        $(".col4").addClass("col-md-4").removeClass("col-md-6");
        $(".col8").addClass("col-md-8").removeClass("col-md-6");
        $(".courses_content").removeClass("col-md-12");
        $(".courses_content").addClass("col-md-10");
        $(".col5").removeClass("col-md-5");
        $(".col5").addClass("col-md-3");
        $(".col12").removeClass("col-md-12");
        $(".col12").addClass("col-md-10");
        $(".col10").addClass("col-md-10");
        $(".col10").removeClass("col-md-12");
        $(".col10").addClass("col-sm-10");
        $(".col10").removeClass("col-sm-12");
        $(".col65").addClass("col-md-6");
        $(".col65").removeClass("col-md-5");
        $(".col65").addClass("col-sm-6");
        $(".col65").removeClass("col-sm-5");
        $(".col67").addClass("col-md-6");
        $(".col67").removeClass("col-md-7");
        $(".col67").addClass("col-sm-6");
        $(".col67").removeClass("col-sm-7");


    }
    $("#menu_button").click(function () {
        if ($("#menu_button").hasClass("menu_opened")) {
            $("#menu>*").fadeOut(100);
            $("#menu_button").removeClass("menu_opened");
            if ($("#menu").hasClass("menu_opened")){
                $("#menu").removeClass("menu_opened");
            }
            if ($("#content").hasClass("menu_opened")){
                $("#content").removeClass("menu_opened");
            }
            if ($(".achievements").hasClass("menu_opened")){
                $(".achievements").removeClass("menu_opened");
            }
            minimizeContent();

        } else {
            $("#menu>*").fadeIn(800);
            $("#menu_button").addClass("menu_opened");
            if (! $("#menu").hasClass("menu_opened")){
                $("#menu").addClass("menu_opened");
            }
            if (! $("#content").hasClass("menu_opened")){
                $("#content").addClass("menu_opened");
            }
            if (! $(".achievements").hasClass("menu_opened")){
                $(".achievements").addClass("menu_opened");
            }
            maximizeContent();
        }
    })
    $(".submenu_item").click(function () {
        if (! $(this).hasClass("active")) {
            $(".submenu_item").removeClass("active");
            $(this).addClass("active");
        }
    })
    $("#submenu_opener").click(function () {
        if ($(".submenu_items").hasClass("sub_closed")){
            $(".submenu_items").removeClass("sub_closed");
            $("#sub_mt").addClass("sub_mt");
        } else {
            $("#sub_mt").removeClass("sub_mt");
            $(".submenu_items").addClass("sub_closed");
        }
    });
    $(".checker").click(function () {
        if ($(this).hasClass("checked")){
            $(this).removeClass("checked");
        } else {
            $(this).addClass("checked");
        }
    });

    $(".video_click").click(function () {
        hidable = $(this).children();
        video = $(this).children()[1];
        if (video.paused) {
            video.play();
            $(hidable[0]).addClass("hideup");
            $(hidable[1]).prop("controls",true);
            $(hidable[2]).addClass("hidedown");
        } else {
            video.pause();
            $(hidable[0]).removeClass("hideup");
            $(hidable[1]).prop("controls",false);
            $(hidable[2]).removeClass("hidedown");
        }
    });
    $(".video").on('ended',function () {
        $($(this).parent()[0].children[0]).removeClass("hideup");
        $($(this).parent()[0].children[1]).prop("controls",true);
        $($(this).parent()[0].children[2]).removeClass("hidedown");
    });

});
//
//
// function vended(arg) {
//     $roman = arg;
//     console.log($roman);
//     $($($roman).parent()[0].children[0]).removeClass("hideup");
//     $($($roman).parent()[0].children[1]).prop("controls",true);
//     $($($roman).parent()[0].children[2]).removeClass("hidedown");
//
// }