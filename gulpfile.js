const gulp   = require('gulp'); // Подключаем Gulp
const sass   = require('gulp-sass');
const concat = require('gulp-concat');
const csso = require('gulp-csso');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('sass',()  =>  // Создаем таск "sass"
    gulp.src(['app/sass/**/*.sass', 'app/sass/**/*.scss']) // Берем источник
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError)) // Преобразуем Sass в CSS посредством gulp-sass
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(csso())
        .pipe(gulp.dest('app/css')) // Выгружаем результата в папку css
);



gulp.task('js', function() {
    return gulp.src([
        'app/js/main.js', // Always at the end
    ])
        .pipe(concat('scripts.min.js'))
        // .pipe(uglify()) // Minify js (opt.)
        .pipe(gulp.dest('app/js/concat'))

});


gulp.task('watch', () => {
        gulp.watch(['app/sass/**/*.sass', 'app/sass/**/*.scss'], ['sass']); // Наблюдение за sass файлами в папке sass
        gulp.watch(['app/libs/**/*.js', 'app/js/*.js'], ['js']);
    }
);

gulp.task('default', ['watch']);
